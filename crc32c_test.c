#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include "crc32c.h"

#define FUNC(foo) do {                          \
        crc = foo(init, msg, strlen(msg));      \
        printf("%20s: 0x%X\n", #foo, crc);      \
    } while (0)

int
main(int argc, char* argv[])
{
    uint32_t init;
    uint32_t crc;

    if (argc == 2) {
        char* msg = argv[1];

        init = crc32c_init();
        FUNC(crc32cSarwate);
        FUNC(crc32cSlicingBy4);
        FUNC(crc32cSlicingBy8);
        FUNC(crc32cHardware32);
        FUNC(crc32cHardware64);
        printf("--------------------\n");

        init = 0;
        FUNC(crc32cSarwate);
        FUNC(crc32cSlicingBy4);
        FUNC(crc32cSlicingBy8);
        FUNC(crc32cHardware32);
        FUNC(crc32cHardware64);
        printf("--------------------\n");

        return 0;
    }
    init = crc32c_init();
    char* abc = "aaaabbbcccc";
    char* a = "aaaa";
    char* b = "bbb";
    char* c = "cccc";

    uint32_t r1 = crc32cSarwate(init, a, strlen(a));
    uint32_t r2 = crc32cSarwate(r1, b, strlen(b));
    uint32_t r3 = crc32cSarwate(r2, c, strlen(c));
    uint32_t r  = crc32cSarwate(init, abc, strlen(abc));
    printf("0x%x == 0x%x\n", r3, r);
    printf("--------------------\n");

    char* abc2 = "aaaaxxxcccc";
    char* b2 = "xxx";
    printf("sizeof =  %lu\n", strlen(c) * 8);
    uint32_t r4 = crc32cSarwate(init, b2, strlen(b2));
    uint32_t rx = crc32cSarwate(init, b, strlen(b));
    r4 = r4 ^ rx;
    uint32_t r5 = __builtin_ia32_crc32si(0, r4);
    r3 ^= r5;

    uint32_t r6 = crc32cSarwate(init, abc2, strlen(abc2));
    printf("0x%x == 0x%x\n", r3, r6);
    printf("--------------------\n");

    char* abc4 = "0000aaaabbbbccccddddeeeeffffgggghhhhiiiijjjjkkkkllll";
    char* abc3 = "0000a234iiiiccccddddeeeeffffgggghhhhiiiijjjjkkkkllll";
    char* ax = "a234iiii";
    char* a2 = "a234";
    char* a0 = "aaaabbbb";
    uint32_t r7 = crc32cSarwate(init, abc3, strlen(abc3));
    r6 = crc32cSarwate(init, abc4, strlen(abc4));

    r4 = crc32cSarwate(init, ax, strlen(ax));
    int r8 = crc32cSarwate(init, a2, strlen(a2));;

    rx = crc32cSarwate(init, a0, strlen(a0));


    char* iiii = "iiii";
    int ry = crc32cSarwate(init, iiii, strlen(iiii));
    r8 = r8 ^ ry;
    r4 = r4 ^ rx;

    /* r5 = __builtin_ia32_crc32si(0, r4); */
    /* r5 = __builtin_ia32_crc32si(0, r5); */

    // patch aaaa -> a234
    int cnt = strlen(abc4)/4 - 3;
    r5  = r4;
    while (cnt--) r5 = crc32cSarwate(0, &r5, 4);

    printf("r5 = 0x%x\n", r5);
    r3 = r6 ^ (r5);

    // patch iiii -> a234
    /* cnt = 3; */
    /* r5  = r8; */
    /* while (cnt--) r5 = crc32cSarwate(0, &r5, 4); */

    /* printf("r5 = 0x%x\n", r5); */

    /* r3 = r3 ^ r5; */

    printf("Replace: 0x%x == 0x%x\n", r3, r7);
    printf("--------------------\n");

    return 0;
}
