CFLAGS  = -msse4.2 -Wall -Wno-sign-compare
#CFLAGS += -O3 -DNDEBUG

SRCS   = crc32c.c crc32c_hardware.c crc32c_software.c
OBJS   = $(SRCS:.c=.o)
TARGET = libcrc32c.a crc32c_test

all: $(TARGET)

crc32c_test: crc32c_test.c libcrc32c.a
	gcc $(CFLAGS) -L. -o $@ $^ -lcrc32c

libcrc32c.a: $(OBJS)
	ar rvs $@ $^

%.o: %.c
	gcc -c $(CFLAGS) -o $@ $^

clean:
	rm -f $(TARGET) $(OBJS)
